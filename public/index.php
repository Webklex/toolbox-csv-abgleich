<?php

/*
* File: index.php
* Category: -
* Author: MSG
* Created: 15.08.15 11:25
* Updated: -
*
* Description:
*  -
*/

require_once '../app/classes/App.php';
$app = new App();

require_once '../app/template/header.php';

$_GET['step'] = (isset($_GET['step'])?$_GET['step']:'home');

switch($_GET['step']){
    case 2:
        require_once '../app/parts/second.php';
        break;
    case 3:
        require_once '../app/parts/third.php';
        break;
    case 'about':
        require_once '../app/parts/about.php';
        break;
    default:
        require_once '../app/parts/first.php';
        break;
}

require_once '../app/template/footer.php';
