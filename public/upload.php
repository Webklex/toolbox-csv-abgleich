<?php

/*
* File: upload.php
* Category: -
* Author: MSG
* Created: 15.08.15 12:03
* Updated: -
*
* Description:
*  -
*/


require_once '../app/classes/App.php';
$app = new App();

$ds          = DIRECTORY_SEPARATOR;
$storeFolder = '../app/uploads';

if (!empty($_FILES)) {
    $tempFile = $_FILES['file']['tmp_name'];
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;
    $targetFile =  $targetPath. session_id().'_'.$_GET['name'];

    move_uploaded_file($tempFile,$targetFile);
}else{
    //ERROR 503
}