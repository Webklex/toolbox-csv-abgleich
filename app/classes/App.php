<?php

/*
* File: App.php
* Category: -
* Author: MSG
* Created: 07.10.15 13:10
* Updated: -
*
* Description:
*  -
*/
  
class App {

    public $config = [];

    public function __construct(){
        $this->init();
    }

    public function init(){
        $this->initAutoload();
        $this->loadConfig();
        $this->initDebug();
        $this->initSession();
        $this->initCache();
    }

    public function initAutoload(){
        spl_autoload_register(function($class){
            $folders = ['classes'];
            $appFolder = dirname(__DIR__);

            foreach($folders as $folder){
                $file = $appFolder.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$class.'.php';
                if(file_exists($file)){
                    require_once $file;
                }
            }
        });
    }

    private function initCache(){
        ob_start();
    }

    private function initSession(){
        session_start();
    }

    private function loadConfig(){
        $configFile = dirname(__DIR__).'/config/config.php';
        if(!file_exists($configFile)){
            throw new Exception('MISSING CONFIG FILE!!');
        }
        $this->config = include($configFile);
    }

    private function initDebug(){
        if($this->config['system']['debug']){
            error_reporting(1);
            ini_set('display_errors', true);
        }else{
            error_reporting(0);
            ini_set('display_errors', false);
        }
    }

    public function __destruct(){
        $content = ob_get_contents();
        ob_end_clean();
        echo $content;
    }
}