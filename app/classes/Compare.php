<?php

/*
* File: Compare.php
* Category: -
* Author: MSG
* Created: 07.10.15 13:27
* Updated: -
*
* Description:
*  -
*/
  
class Compare {

    private $fileLeft = '';
    private $fileRight = '';

    private $lines = null;
    private $final = null;

    public function __construct(){
        $this->fileLeft = dirname(__DIR__).'/uploads/'.session_id().'_FIRST';
        $this->fileRight = dirname(__DIR__).'/uploads/'.session_id().'_SECOND';

        $this->checkFiles();
    }

    public function run(){
        $this->readFiles($this->fileLeft, 'FIRST');
        $this->readFiles($this->fileRight, 'SECOND');
        $this->headlines();
        $this->analyze();
    }

    private function mergeArrays($arr1, $arr2){
        foreach($arr2 as $val){
            $arr1[] = $val;
        }
        return $arr1;
    }

    private function checkFiles(){
        if(
            !file_exists($this->fileLeft) ||
            !file_exists($this->fileRight)){

            header('Location: /?step=1');
            $_SESSION['flash'][] = [
                'type' => 'danger',
                'msg' => 'Bitte laden Sie mindestens zwei Datein hoch'
            ];
        }
    }

    private function readFiles($file, $type){
        $handle = @fopen($file, "r");
        if ($handle) {
            while((($buffer = fgetcsv($handle, 0, $_POST['seperator_'.$type])) !== false)){
                $this->lines[$type][] = $buffer;
            }
        }
    }


    private function headlines(){
        if($_POST['keep_headlines'] == 'on'){
            $final[] = $this->mergeArrays($this->lines['FIRST'][0],$this->lines['SECOND'][0]);
        }
    }

    private function analyze(){
        foreach($this->lines['FIRST'] as $primary => $pLine){
            $match = false;
            foreach($this->lines['SECOND'] as $secondary => $sLine){
                if($pLine[$_POST['seperator_FIRST_VALUE']] == $sLine[$_POST['seperator_SECOND_VALUE']]){
                    $match = $sLine;
                    break;
                }
            }

            if($match){
                if($_POST['remove_matches'] != 'on'){
                    $this->final[] = $this->mergeArrays($pLine, $match);
                }
            }elseif($_POST['remove_missmatches'] != 'on'){
                $this->final[] = $pLine;
            }
        }
    }

    public function render($download = false){
        if($download){
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename=abgleich_'.date('d_m_Y_H_i').'.csv');
            header('Pragma: no-cache');
        }

        foreach($this->final as $line){
            echo implode($_POST['seperator_FIRST'], $line)."\n";
        }
    }

    public function __destruct(){
        unlink($this->fileLeft);
        unlink($this->fileRight);
    }
}