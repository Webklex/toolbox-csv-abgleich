<?php

/*
* File: first.php
* Category: -
* Author: MSG
* Created: 15.08.15 12:12
* Updated: -
*
* Description:
*  -
*/

?>
<script>
	
	(function(){
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            acceptedFiles: '.csv'
        };
	Dropzone.autoDiscover = false;
	
	    $(document).ready(function(){

		jQuery("#dropzoneFirst").dropzone({ url: "/upload.php?name=FIRST" });
		jQuery("#dropzoneSecond").dropzone({ url: "/upload.php?name=SECOND" });
	    });
    })();
</script>

<div class="container" style="min-height: 350px;;">
    <div class="row">
        <div class="col-xs-7">
            <h3 class="footer-title">Datein auswählen</h3>

            <div class="row">
                <div class="col-xs-6">
                    <form action="/file-upload" class="dropzone" id="dropzoneFirst">
  <div class="fallback">
    <input name="file" type="file" />
  </div></form>
                </div>
                <div class="col-xs-6" style="padding-left: 16px">
                    <form action="/file-upload" class="dropzone" id="dropzoneSecond">
  <div class="fallback">
    <input name="file" type="file" />
  </div></form>
                </div>
            </div>
            <div class="row">
                <br />
            </div>

            <form method="POST" action="/?step=2">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Trennzeichen Datei I</label>
                            <input type="text" value=";" placeholder="" name="seperator_FIRST" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-6" style="padding-left: 16px">
                        <div class="form-group">
                            <label>Trennzeichen Datei II</label>
                            <input type="text" value=";" placeholder="" name="seperator_SECOND" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <button type="submit" name="submit" class="btn btn-lg btn-primary">
                            Fortfahren <span class="fui-arrow-right"></span>
                        </button>
                    </div>
                </div>
            </form>

        </div> <!-- /col-xs-7 -->
    </div>
</div>
