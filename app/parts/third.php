<?php

/*
* File: third.php
* Category: -
* Author: MSG
* Created: 15.08.15 13:13
* Updated: -
*
* Description:
*  -
*/

session_start();

if(!file_exists('../app/uploads/'.session_id().'_FIRST') || !file_exists('../app/uploads/'.session_id().'_SECOND')){
    header('Location: /?step=1');
    $_SESSION['flash'][] = [
        'type' => 'danger',
        'msg' => 'Bitte laden Sie mindestens zwei Datein hoch'
    ];
}


?>
<div class="container" style="min-height: 350px;;">
    <div class="row">
        <div class="col-xs-7 text-center">
            <h1 class="footer-title">Das War's!</h1>

            <p>Nun haben wir alles zusammen. Gut gemacht!</p>

            <br />
            <br />
            <form method="POST" action="/download.php">
                <?php
                foreach($_POST as $name => $value){
                    echo "<input type='hidden' name='$name' value='$value' />";
                }
                ?>
                <button class="btn btn-primary btn-lg" type="submit">
                    Generieren & Download
                </button>
            </form>
        </div> <!-- /col-xs-7 -->

    </div>
</div>