<?php

/*
* File: about.php
* Category: -
* Author: MSG
* Created: 15.08.15 14:36
* Updated: -
*
* Description:
*  -
*/
  
 ?>
<div class="container" style="min-height: 350px;;">
    <div class="row">
        <div class="col-xs-7">
            <h1 class="footer-title">Über die <i>Toolbox</i></h1>

            <p>
                Dieses Tool wurde aus eigenem Bedarf hin entwickelt. Es wird keine Garantie über die Richtigkeit oder
                die Datensicherheit gegeben. Das Tool kann auf eine Gefahr hin gern verwendet werden.
            </p>
            <p>
                <strong>Credits:</strong><br />
                Flat UI: <a href="http://designmodo.com">designmodo.com</a>
            </p>
        </div> <!-- /col-xs-7 -->

        <div class="col-xs-4">
            <div class="footer-banner">
                <h3 class="footer-title">Verwantwortlich</h3>
                <p>
                    Malte Goldenbaum<br />
                    Mittelweg 32<br />
                    25336 Elmshorn<br />
                    <small>
                        <i>Telefon / Mail nur auf Anfrage.</i>
                    </small>
                </p>
                Fragen / Probleme:
                <a href="http://twitter.com/thatMSG" target="_blank">twitter.com/thatMSG</a>
            </div>
        </div>
    </div>
</div>