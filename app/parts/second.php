<?php

/*
* File: second.php
* Category: -
* Author: MSG
* Created: 15.08.15 12:22
* Updated: -
*
* Description:
*  -
*/

session_start();

if(!file_exists('../app/uploads/'.session_id().'_FIRST') || !file_exists('../app/uploads/'.session_id().'_SECOND')){
    header('Location: /?step=1');
    $_SESSION['flash'][] = [
        'type' => 'danger',
        'msg' => 'Bitte laden Sie mindestens zwei Datein hoch'
    ];
}

$files = [
    'FIRST'=>'../app/uploads/'.session_id().'_FIRST',
    'SECOND'=>'../app/uploads/'.session_id().'_SECOND'
];

$fields = null;

foreach($files as $key => $file){
    $handle = @fopen($file, "r");
    if ($handle) {
        if((($buffer = fgets($handle, 4096)) !== false)){
            $fields[$key] = explode($_POST['seperator_'.$key], $buffer);
        }
    }
}

?>
<div class="container" style="min-height: 350px;;">
    <div class="row">
        <div class="col-xs-7">
            <h3 class="footer-title">Zugehörigkeit klären</h3>

            <form method="POST" action="/?step=3">
                <input type="hidden" name="seperator_FIRST" value="<?=$_POST['seperator_FIRST']?>" />
                <input type="hidden" name="seperator_SECOND" value="<?=$_POST['seperator_SECOND']?>" />
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Elemente der Datei I</label>
                            <select name="seperator_FIRST_VALUE" class="form-control">
                                <?php
                                foreach($fields['FIRST'] as $id => $field){
                                    echo '<option value="'.$id.'">'.strtoupper(str_replace('_', ' ', $field)).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6" style="padding-left: 16px">
                        <div class="form-group">
                            <label>Elemente der Datei II</label>
                            <select name="seperator_SECOND_VALUE" class="form-control">
                                <?php
                                foreach($fields['SECOND'] as $id => $field){
                                    echo '<option value="'.$id.'">'.strtoupper(str_replace('_', ' ', $field)).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4" style="padding-left: 16px">
                        <div class="form-group">
                            <label>Kopfzeilen behalten?</label>
                            <div class="bootstrap-switch-square">
                                <input type="checkbox" name="keep_headlines" checked data-toggle="switch" id="custom-switch-03" data-on-text="<span>Ja</span>" data-off-text="<span>Nein</span>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" style="padding-left: 16px">
                        <div class="form-group">
                            <label>Mismatches entfernen?</label>
                            <div class="bootstrap-switch-square">
                                <input type="checkbox" name="remove_missmatches" checked data-toggle="switch" id="custom-switch-03" data-on-text="<span>Ja</span>" data-off-text="<span>Nein</span>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4" style="padding-left: 16px">
                        <div class="form-group">
                            <label>Matches entfernen?</label>
                            <div class="bootstrap-switch-square">
                                <input type="checkbox" name="remove_matches" data-toggle="switch" id="custom-switch-03" data-on-text="<span>Ja</span>" data-off-text="<span>Nein</span>" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <br />
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <button type="submit" name="submit" class="btn btn-lg btn-primary">
                            Fortfahren <span class="fui-arrow-right"></span>
                        </button>
                    </div>
                </div>
            </form>
        </div> <!-- /col-xs-7 -->
    </div>
</div>