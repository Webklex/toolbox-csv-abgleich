<?php

/*
* File: header.php
* Category: -
* Author: MSG
* Created: 15.08.15 11:19
* Updated: -
*
* Description:
*  -
*/
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Toolbox</title>
    <meta name="description" content="Toolbox"/>

    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

    <!-- Loading Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Loading Flat UI -->
    <link href="/templates/Flat-UI/dist/css/flat-ui.min.css" rel="stylesheet">
    <link href="/templates/Flat-UI/docs/assets/css/demo.css" rel="stylesheet">
    
    <link href="https://cdn.rawgit.com/enyo/dropzone/master/dist/basic.css" rel="stylesheet">
    <link href="https://cdn.rawgit.com/enyo/dropzone/master/dist/dropzone.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <link rel="shortcut icon" href="img/favicon.ico">
    
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdn.rawgit.com/enyo/dropzone/master/dist/dropzone.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.14.1/dist/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="http://vjs.zencdn.net/6.6.3/video.js"></script>

<script src="/templates/Flat-UI/dist/scripts/flat-ui.min.js"></script>


<script src="/templates/Flat-UI/docs/assets/js/application.js"></script>


</head>
<body>
<div class="container">
    <div class="row"><br /></div>

    <div class="row">
        <div class="col-xs-12">
        <?php
        if(isset($_SESSION['flash'] )){
            foreach($_SESSION['flash'] as $key => $flash){
                ?>
                <div class="alert alert-<?=$flash['type']?>" role="alert"><?=$flash['msg']?></div>
                <?php
                unset($_SESSION['flash'][$key]);
            }
        }
        ?>
    </div>
    </div>
